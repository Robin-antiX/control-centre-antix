��    T      �  q   \            !     3     E     R     k     s     �     �     �     �     �     �     �                4     O     k     �  "   �     �     �     �     �     �     �     	     	     /	     C	     U	     q	     �	     �	     �	     �	     �	     �	     �	     �	     
     
     %
     5
     A
     Y
     m
     u
     �
     �
     �
     �
     �
     �
               ,     D     Q     ]     s     �     �     �     �     �     �     �          /     G     N     W     j     �     �  
   �     �     �     �     �     �       �       �     �     �     �     �     	          /     ?     L     Y     l          �     �     �     �     �     �     �            	   %     /     D     W     j     ~     �     �     �     �     �  	   �     �                    ;     P     `     p  	   w     �     �     �     �     �     �     �     �               0     F     S     `     r  	   �     �     �     �     �     �     �      �          %     =     V     l     s     z     �     �     �     �     �     �     �     �     �              '         G          K   +       7       
      *       C           D       =       .                    L   E       B   3      @   M   F          ,          1      &   I              N   /   (         9       S   5           ?      0   <   Q             H   4   2                   %         R       6   :      	   ;       P   $   )   T             A   -   !   >      "             J                    #   O   8     DisconnectShares 1-to-1 Assistance 1-to-1 Voice ADSL/PPPOE Configuration Adblock Adjust Mixer Alsamixer Equalizer Alternatives Configurator Backlight Brightness Boot Repair Change Slim Background Choose Startup Services Choose Wallpaper Codecs Installer Configure Automount Configure Live Persistence ConnectShares Configuration Customize Look and Feel Desktop Dial-Up Configuaration (GNOME PPP) Disk Manager Disks Drivers Droopy (File Sharing) Edit Bootloader Menu Edit Config Files Edit Exclude Files Edit Fluxbox Settings Edit IceWM Settings Edit JWM Settings Edit System Monitor (Conky) Firewall Configuration Hardware ISO Snapshot Image a Partition Install antiX Linux Live Live USB Maker (cli) Live USB Maker (gui) Live-USB Kernel Updater Login Manager Maintenance Manage Packages Menu Editor Mount Connected Devices Mouse Configuration Network Network Assistant Network Interfaces (Ceni) Nvidia Driver Installer PC Information Package Installer Partition a Drive Password Prompt (su/sudo) Preferred Applications Print Settings Remaster-Customize Live Repo Manager SSH Conduit Save Root Persistence Select wifi Application Session Set Auto-Login Set Date and Time Set Font Size Set Grub Boot Image (png only) Set Screen Blanking Set Screen Resolution (ARandR) Set System Keyboard Layout Set Up Live Persistence Shares Software Sound Card Chooser Synchronize Directories System System Backup Test Sound User Desktop-Session User Manager WPA Supplicant Configuration WiFi Connect (Connman) antiX Updater antiX autoremove Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2011-10-27 11:08+0000
Last-Translator: Xie Yanbo <xieyanbo@gmail.com>, 2023
Language-Team: Chinese (China) (http://app.transifex.com/anticapitalista/antix-development/language/zh_CN/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: zh_CN
Plural-Forms: nplurals=1; plural=0;
 关闭分享连接 一对一协助 一对一语音通话 拨号连接设置 广告终结者 调整混合器 声卡调节均衡器 备选配置器 背光亮度 引导修复 选择基本背景 选择启动服务 选择壁纸 安装 解码器 自动安装配置 配置 Live 持久化 分享连接设置 定制外观和感受 桌面 拨号设置(GNOME PPP) 磁盘管理器 磁盘 驱动器 Droopy(文件分享) 编辑引导菜单 编辑配置文件 编辑Exclude文件 更改Fluxbox设置 修改IceWM设置 修改JVM设置 修改系统监视器(Conky) 防火墙配置 硬件 ISO快照 镜像一个分区 安装 antiX Linux 流 USB系统制作（命令行） USB系统制作(GUI) USB系统升级 登录管理器 维护 管理包 菜单编辑器 挂载已连接的设备 鼠标设置 网络 网络辅助程序 网络连接(Ceni) NVIDIA驱动安装器 系统信息 软件包安装程序 分区一个磁盘 密码提示(su/sudo) 常用程序 输出设置 重制定制 Live 配置管理器 SSH隧道 保存root持久化 选择 wifi 应用 会话 设置自动登录 修改日期和时间 更改字体大小 修改Grub启动镜像(png only)  背景设置 修改分辨率(ARandR) 设置系统键盘输出 设置 Live 持久化 分享 软件 声卡设置 同步目录 系统 系统备份 测试音频 用户桌面会话 用户管理 WPA求助设置 WiFi连接(连接器) antiX 更新器 antiX 自动删除 